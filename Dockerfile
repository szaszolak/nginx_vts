FROM nginx:1.15.12 AS builder

ENV NGINX_VERSION 1.15.12
ENV VTS_VERSION 0.1.18

RUN apt-get update &&  apt-get install --no-install-recommends --no-install-suggests -y \
  gnupg1 \
  ca-certificates  \
  gcc \
  libc-dev \
  make \
  openssl\
  curl \
  gnupg \
  wget \
  libpcre3 libpcre3-dev \
  libghc-zlib-dev

RUN wget "http://nginx.org/download/nginx-${NGINX_VERSION}.tar.gz" -O nginx.tar.gz && \
    wget "https://github.com/vozlt/nginx-module-vts/archive/v${VTS_VERSION}.tar.gz" -O vts.tar.gz

# Reuse same cli arguments as the nginx:1.14.2 image used to build
RUN CONFARGS=$(nginx -V 2>&1 | sed -n -e 's/^.*arguments: //p') \
	tar -zxC /usr/src -f nginx.tar.gz && \
  tar -xzvf "vts.tar.gz" && \
  VTS_DIR="$(pwd)/nginx-module-vts-${VTS_VERSION}/" && \
  cd /usr/src/nginx-$NGINX_VERSION && \
  ./configure --with-compat $CONFARGS --add-dynamic-module=$VTS_DIR && \
  make && make install

FROM nginx:1.15.12

# Copy compiled static module
COPY --from=builder /usr/local/nginx/modules/ngx_http_vhost_traffic_status_module.so /usr/local/nginx/modules/ngx_http_vhost_traffic_status_module.so

EXPOSE 80
STOPSIGNAL SIGTERM
CMD ["nginx", "-g", "daemon off;"]